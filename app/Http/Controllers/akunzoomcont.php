<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\akun_zoom;
use App\Models\peminjaman;
use Illuminate\Support\Facades\DB;

class akunzoomcont extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        // return view('vakunzoom');
        $azoom = akun_zoom::all();
        return view('vakunzoom',['akunzoom'=>$azoom]);

        // $subpeminjaman =peminjaman::select('id_akun_zoom','nama_kegiatan','tanggal','jam','durasi','status_pinjam',);
        // $azoom = akun_zoom::leftjoinSub($subpeminjaman,'subjekpeminjaman', function($join){
        //     $join->on('akun_zooms.id', '=', 'subjekpeminjaman.id_akun_zoom');
        // })->get();

        // $azoom = peminjaman::leftJoin('akun_zooms', 'peminjamen.id_akun_zoom','=', 'akun_zooms.id')
        // ->select('akun_zooms.*', 'peminjamen.nama_kegiatan', 'peminjamen.tanggal', 'peminjamen.jam', 'peminjamen.durasi', 'peminjamen.status_pinjam')
        // ->get();

  
        return view('/vakunzoom', [ 
        'akunzoom' => $azoom,
        ]);


        // $azoom = DB::table('akun_zooms')
        //             ->leftJoin('peminjamen', 'akun_zooms.id', '=', 'peminjamen.id_akun_zoom')
        //             ->select('akun_zooms.*', 'peminjamen.nama_kegiatan', 'peminjamen.tanggal', 'peminjamen.jam', 'peminjamen.durasi', 'peminjamen.status_pinjam',   )
        //             ->where('peminjamen.deleted_at'is_null)
        //             ->get();
        // //$jumlah_transaksi = DB::table('transaksi')->count();
        // return view('/vakunzoom', [
        //     //'id_page' => 'transaksi',
        //     'akunzoom' => $azoom,
        //     //'jml_transaksi' => $jumlah_transaksi
        // ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('vtambahakunzoom');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $azoom = new akun_zoom();
        $azoom->email = $request->email;
        $azoom->password = $request->password;
        $azoom->kapasitas = $request->kapasitas;

        $azoom->save();

        return redirect('/akunzoom');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $azoom = akun_zoom::where('id',$id)->first();
        return view('veditakunzoom',['akunzoom'=>$azoom]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $azoom  = akun_zoom::find($id);
        $azoom->email = $request->email;
        $azoom->password = $request->password;
        $azoom->kapasitas = $request->kapasitas;

        $azoom->save();
        return redirect('/akunzoom');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $azoom = akun_zoom::find($id);
        $azoom->delete();
        
        return redirect('/akunzoom');
    }
}
