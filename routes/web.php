<?php

// use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/peminjaman', function () {
//     return view('vpeminjaman');
// });
// Route::get('/jadwalapproved', function () {
//     return view('vjadwalapproved');
// });
// Route::get('/akunzoom', function () {
//     return view('vakunzoom');
// });
// Route::get('/requestpeminjaman', function () {
//     return view('vrequestpeminjaman');
// });
// Route::get('/riwayatpeminjaman', function () {
//     return view('vriwayatpeminjaman');
// });
// Route::get('/tambahakunzoom', function () {
//     return view('vtambahakunzoom');
// });


// Route::get('/iniforgotpassword', function () {
//     return view('vforgotpassword');
// });
Route::get('/iniregister', function () {
    return view('vregister');
});


// Route::get('/index', function () {
//     return view('index');
// });

// login
Route::get('/inilogin', function () {
    return view('login');
})->name('inilogin');
// Route::get('/login',[App\Http\Controllers\logincont::class,'postLogin'])->name('login');
// Route::match(['get','post'],'/postlogin',[App\Http\Controllers\logincont::class,'postLogin']);
Route::post('/postlogin',[App\Http\Controllers\logincont::class,'postLogin']);
Route::get('/logout',[App\Http\Controllers\logincont::class,'logout']);
Route::get('/iniregister',[App\Http\Controllers\logincont::class,'create']);
Route::post('/register/store',[App\Http\Controllers\logincont::class,'store']);










// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['middleware' => ['auth','cekLevel:staff']], function (){
    Route::get('/peminjaman',[App\Http\Controllers\peminjamancont::class,'index']);
    Route::get('/peminjaman/edit{id}',[App\Http\Controllers\peminjamancont::class,'edit']);
    Route::post('/peminjaman/update/{id}',[App\Http\Controllers\peminjamancont::class,'update']);
    Route::get('/peminjaman/destroy/{id}',[App\Http\Controllers\peminjamancont::class,'destroy']);
    Route::get('/recentpeminjaman',[App\Http\Controllers\peminjamancont::class,'recent']);
    Route::get('/akunzoom/edit/{id}',[App\Http\Controllers\akunzoomcont::class,'edit']);
    Route::post('/akunzoom/update/{id}',[App\Http\Controllers\akunzoomcont::class,'update']);
    Route::get('/akunzoom/destroy/{id}',[App\Http\Controllers\akunzoomcont::class,'destroy']);
    Route::get('/tambahakunzoom',[App\Http\Controllers\akunzoomcont::class,'create']);
    Route::post('/tambahakunzoom/store',[App\Http\Controllers\akunzoomcont::class,'store']);

});

Route::group(['middleware' => ['auth','cekLevel:mahasiswa']], function (){
    Route::get('/riwayatpeminjaman',[App\Http\Controllers\riwayatpeminjamancont::class,'index']);
    Route::get('/riwayatpeminjaman/destroy/{id}',[App\Http\Controllers\riwayatpeminjamancont::class,'destroy']);
    Route::get('/requestpeminjaman',[App\Http\Controllers\requestpeminjamancont::class,'create']);
    Route::post('/requestpeminjaman/store',[App\Http\Controllers\requestpeminjamancont::class,'store']);
    Route::get('/recentriwayat',[App\Http\Controllers\riwayatpeminjamancont::class,'recent']);
});

Route::group(['middleware' => ['auth','cekLevel:staff,mahasiswa']], function (){
    Route::get('/akunzoom',[App\Http\Controllers\akunzoomcont::class,'index']);
    Route::get('/index',[App\Http\Controllers\berandacont::class,'index']);
    Route::get('/jadwalapproved',[App\Http\Controllers\jadwalapprovedcont::class,'index']);
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
