@extends('layout2.template')
@section('title','Edit Akun Zoom')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Edit Akun Zoom</h3></div>
                    <div class="card-body">
                        <form action="/akunzoom/update/{{$akunzoom->id}}" method="POST">
                            {{ csrf_field() }}
                            <div><input type="hidden" name="id" value="{{ $akunzoom->id }}"> <br /></div>
                            <div class="form-floating mb-3">
                                <input name="email" value="{{$akunzoom->email}}" required="required" class="form-control" type="text"  />
                                <label>Email</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="password" value="{{$akunzoom->password}}" required="required" class="form-control" type="text"  />
                                <label>Password</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="kapasitas" value="{{$akunzoom->kapasitas}}" required="required" class="form-control" type="text" />
                                <label>Kapasitas</label>
                            </div>
                            <div class="mt-4 mb-0">
                                <input class="d-grid btn btn-primary btn-block " value="Update" type="submit">
                            </div>
                        </form>
                   
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
