@extends('layout2.template')
@section('title','Tambah Akun Zoom')
@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Tambah Akun Zoom</h3></div>
                    <div class="card-body">
                        <form action="/peminjaman/update/{{$peminjaman->id}}" method="POST">
                            {{ csrf_field() }}
                            <div><input type="hidden" name="id" value="{{ $peminjaman->id }}"><br /></div>
                            <div class="form-floating mb-3">
                                <select class="selectpicker form-control" data-live-search="false"  name="nama_kegiatan" id="nama_kegiatan" required>
                                    <option value="{{$peminjaman->id}}">{{$peminjaman->nama_kegiatan}}</option>
                                </select>
                                <label>Nama Kegiatan</label>
                            </div>

                            <div class="form-floating mb-3">
                                <select class="selectpicker form-control"  data-live-search="false"  class="form-control" name="status_pinjam" id="status_pinjam" required>
                                    <option value="{{$peminjaman->status_pinjam}}">{{ $peminjaman->status_pinjam}}</option>
                                    <option value="approved">approved</option>
                                    <option value="rejected">rejected</option>
                                </select>
                                <label>Status Pinjam</label>
                            </div>

                            <div class="form-floating mb-3">
                                <input name="catatan_staf" class="form-control" type="text" placeholder="Catatan Staff" required/>
                                <label>Catatan Staff</label>
                            </div>
                            
                            <div class="mt-4 mb-0">
                                <input class="d-grid btn btn-primary btn-block " value="Update" type="submit">
    
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
