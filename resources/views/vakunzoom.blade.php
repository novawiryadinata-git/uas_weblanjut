@extends('layout.template')
@section('title','Akun Zoom')
@section('main')

            <div class="container-fluid px-4">
                <h1 class="mt-4">Akun Zoom</h1>

                <div class="card mb-4">
                    <div class="card-header d-flex align-items-center justify-content-between small">
                        <div>
                            <i class="fas fa-table me-1"></i>
                            Akun Zoom
                        </div>
                        @if (auth()->user()->level=='staff')
                        <div>
                            <a href="/tambahakunzoom" class="btn btn-primary "><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
                        </div>
                        @endif

                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    @if (auth()->user()->level=='staff')
                                    <th>Password</th>
                                    @endif
                                    <th>Kapasitas</th>
                                    @if (auth()->user()->level=='staff')
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($akunzoom as $azoom)
                            <tr>
                                <td> {{ $azoom->email }} </td>
                                @if (auth()->user()->level=='staff')
                                    <td> {{ $azoom->password }}</td>
                                @endif
                                <td> {{ $azoom->kapasitas }} </td>
                            
                                
                                @if (auth()->user()->level=='staff')
                                <td style="text-align: center;" class="card-header d-flex align-items-center justify-content-between small"  >
                                    <a href="/akunzoom/edit/{{$azoom->id}}" class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    <a href="/akunzoom/destroy/{{$azoom->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                                @endif
                            </tr>

                        @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


@endsection
