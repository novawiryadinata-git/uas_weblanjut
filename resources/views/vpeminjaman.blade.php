@extends('layout.template')
@section('title','Data Peminjaman')
@section('main')

            <div class="container-fluid px-4">
                <h1 class="mt-4">Data Peminjaman</h1>

                <div class="card mb-4">
                    <div class="card-header d-flex align-items-center justify-content-between small">
                        <div>
                            <i class="fas fa-table me-1"></i>
                            Data Peminjaman
                        </div>
                        <div>
                            <a href="/recentpeminjaman" class="btn btn-primary "><i class="fa fa-undo" aria-hidden="true"></i> Recently Delete</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Nama Kegiatan</th>
                                    <th>Deskripsi</th>
                                    <th>Tanggal</th>
                                    <th>Jam</th>
                                    <th>Durasi</th>
                                    <th>Catatan Staf</th>
                                    <th>Status Pinjam</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($peminjamen as $peminjaman)
                                <tr>
                                    <td> {{ $peminjaman->name }} </td>
                                    <td> {{ $peminjaman->email }} </td>
                                    <td> {{ $peminjaman->nama_kegiatan }} </td>
                                    <td> {{ $peminjaman->deskripsi }} </td>
                                    <td> {{ $peminjaman->tanggal }} </td>
                                    <td> {{ $peminjaman->jam }} </td>
                                    <td> {{ $peminjaman->durasi }} </td>
                                    <td> {{ $peminjaman->catatan_staf }} </td>
                                    @if($peminjaman->status_pinjam == 'approved')
                                        <td class="text-success"> {{ $peminjaman->status_pinjam }} </td>
                                    @elseif($peminjaman->status_pinjam == 'rejected')
                                        <td class="text-danger"> {{ $peminjaman->status_pinjam }} </td>
                                    @else
                                        <td class="text-warning"> {{ $peminjaman->status_pinjam }} </td>
                                    @endif
                                    <td style="text-align: center;" class="card-header d-flex align-items-center justify-content-between small"  >
                                    <div class="d-flex justify-content-center">
                                            @if($peminjaman->status_pinjam == 'approved')
                                            <a href="/peminjaman/destroy/{{$peminjaman->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>    
                                            <a href="/peminjaman/edit{{$peminjaman->id}}" class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a>

                                            @elseif($peminjaman->status_pinjam == 'rejected')
                                            <a href="/peminjaman/destroy/{{$peminjaman->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>    
                                            <a href="/peminjaman/edit{{$peminjaman->id}}" class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                            
                                            @else
                                            <a href="/peminjaman/edit{{$peminjaman->id}}" class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                            @endif
                                        </div> 
                                    </td>
                                </tr>

                            @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


@endsection
